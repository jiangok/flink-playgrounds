export AVRO_TOOL_JAR=~/avro/avro-tools-1.8.2.jar
export AVRO_SCHEMA_PATH=src/main/avro
export AVRO_CODE_PATH=src/main/java

function rebuild {
  . scripts/ops.sh
  generateAvroPojo

  docker-compose down
  mvn install -DskipTests
  docker-compose build
  docker-compose up -d
}



function generateAvroPojo {
  java -jar $AVRO_TOOL_JAR idl2schemata ${AVRO_SCHEMA_PATH}/test.avdl ${AVRO_SCHEMA_PATH}
  java -jar $AVRO_TOOL_JAR compile schema ${AVRO_SCHEMA_PATH}/Person.avsc ${AVRO_CODE_PATH}
  java -jar $AVRO_TOOL_JAR fromjson --schema-file ${AVRO_SCHEMA_PATH}/Person.avsc ${AVRO_SCHEMA_PATH}/Person.json > data/Person.avro
}

function mysql {
  docker-compose exec mysql mysql -Dsql-demo -usql-demo -pdemo-sql
}

function flink {
  docker-compose exec mysql mysql -Dsql-demo -usql-demo -pdemo-sql
}

function list_services {
  docker-compose ps --services
}

# cli taskmanager
function cli {
   docker-compose exec $1 /bin/bash
}

# log jobmanager
function log {
  docker-compose logs $1
}

# up jobmanager
function up {
  docker-compose up $1
}