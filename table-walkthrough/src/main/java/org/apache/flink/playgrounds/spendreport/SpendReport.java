/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.flink.playgrounds.spendreport;

import org.apache.flink.table.api.*;
import org.apache.flink.table.expressions.TimeIntervalUnit;

import static org.apache.flink.table.api.Expressions.*;

public class SpendReport {
    public static void main(String[] args) throws Exception {
        EnvironmentSettings settings = EnvironmentSettings.newInstance().build();
        TableEnvironment tEnv = TableEnvironment.create(settings);

        localavro_mysql(tEnv);
    }

    public static void Memory_Mysql(TableEnvironment tEnv) {
        System.out.println("Memory_Mysql...");
        Table mem = tEnv.fromValues(
                DataTypes.ROW(
                        DataTypes.FIELD("id", DataTypes.INT()),
                        DataTypes.FIELD("name", DataTypes.STRING())
                ),
                row(1, "abc"),
                row(2, "ded")
        );

        tEnv.executeSql("CREATE TABLE memory_mysql (\n" +
                "    id INT,\n" +
                "    name     STRING\n," +
                "    PRIMARY KEY (id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'memory_mysql',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        mem.where($("id").isGreater(1)).executeInsert("memory_mysql");
    }

    public static void kafka_mysql(TableEnvironment tEnv) {
        System.out.println("kafka_mysql...");

        tEnv.executeSql("CREATE TABLE transactions (\n" +
                "    account_id  BIGINT,\n" +
                "    amount      BIGINT,\n" +
                "    transaction_time TIMESTAMP(3),\n" +
                "    WATERMARK FOR transaction_time AS transaction_time - INTERVAL '5' SECOND\n" +
                ") WITH (\n" +
                "    'connector' = 'kafka',\n" +
                "    'topic'     = 'transactions',\n" +
                "    'properties.bootstrap.servers' = 'kafka:9092',\n" +
                "    'format'    = 'csv'\n" +
                ")");

        tEnv.executeSql("CREATE TABLE spend_report (\n" +
                "    account_id BIGINT,\n" +
                "    amount     BIGINT\n," +
                "    transaction_time TIMESTAMP(3),\n" +
                "    PRIMARY KEY (account_id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'spend_report2',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        Table transactions = tEnv.from("transactions");
        transactions.select(
                $("account_id"),
                $("transaction_time").floor(TimeIntervalUnit.HOUR).as("log_ts"),
                $("amount"))
                .groupBy($("account_id"), $("log_ts"))
                .select(
                        $("account_id"),
                        $("log_ts"),
                        $("amount").sum().as("amount"))
                .executeInsert("spend_report");
    }

    public static void localparquet_mysql(TableEnvironment tEnv) {
        System.out.println("localparquet_mysql...");

        tEnv.executeSql("CREATE TABLE people (\n" +
                "    id  INT,\n" +
                "    name STRING\n" +
                ") WITH (\n" +
                "    'connector' = 'filesystem',\n" +
                "    'path'     = 'file:///data/test.parquet',\n" +
                "    'format'    = 'parquet',\n" +
                "    'properties.bootstrap.servers' = 'kafka:9092'\n" +
                ")");

        tEnv.executeSql("CREATE TABLE memory_mysql (\n" +
                "    id INT,\n" +
                "    name     STRING\n," +
                "    PRIMARY KEY (id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'memory_mysql',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        Table people = tEnv.from("people");
        people.where($("id").isGreater(1)).executeInsert("memory_mysql");
    }

    public static void localcsv_mysql(TableEnvironment tEnv) {
        System.out.println("localcsv_mysql...");

        tEnv.executeSql("CREATE TABLE people (\n" +
                "    id  STRING,\n" +
                "    name STRING\n" +
                ") WITH (\n" +
                "    'connector' = 'filesystem',\n" +
                "    'path'     = 'file:///data/test.csv',\n" +
                "    'format'    = 'csv',\n" +
                "    'properties.bootstrap.servers' = 'kafka:9092'\n" +
                ")");

        tEnv.executeSql("CREATE TABLE csv_table (\n" +
                "    id STRING,\n" +
                "    name     STRING\n," +
                "    PRIMARY KEY (id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'csv_table',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        Table people = tEnv.from("people");
        people.executeInsert("csv_table");
    }

    public static void localjson_mysql(TableEnvironment tEnv) {
        System.out.println("localcsv_mysql...");

        tEnv.executeSql("CREATE TABLE people (\n" +
                "    id  INT,\n" +
                "    name STRING\n" +
                ") WITH (\n" +
                "    'connector' = 'filesystem',\n" +
                "    'path'     = 'file:///data/test.json',\n" +
                "    'format'    = 'json',\n" +
                "    'properties.bootstrap.servers' = 'kafka:9092'\n" +
                ")");

        tEnv.executeSql("CREATE TABLE memory_mysql (\n" +
                "    id INT,\n" +
                "    name     STRING\n," +
                "    PRIMARY KEY (id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'memory_mysql',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        Table people = tEnv.from("people");
        people.executeInsert("memory_mysql");
    }

    public static void localavro_mysql(TableEnvironment tEnv) {
        System.out.println("localavro_mysql...");

        tEnv.executeSql("CREATE TABLE people (\n" +
                "    id  INT,\n" +
                "    name STRING\n" +
                ") WITH (\n" +
                "    'connector' = 'filesystem',\n" +
                "    'path'     = '/data/Person.avro',\n" +
                "    'format'    = 'avro',\n" +
                "    'properties.bootstrap.servers' = 'kafka:9092'\n" +
                ")");

        tEnv.executeSql("CREATE TABLE memory_mysql (\n" +
                "    id INT,\n" +
                "    name     STRING\n," +
                "    PRIMARY KEY (id) NOT ENFORCED" +
                ") WITH (\n" +
                "  'connector'  = 'jdbc',\n" +
                "  'url'        = 'jdbc:mysql://mysql:3306/sql-demo',\n" +
                "  'table-name' = 'memory_mysql',\n" +
                "  'driver'     = 'com.mysql.jdbc.Driver',\n" +
                "  'username'   = 'sql-demo',\n" +
                "  'password'   = 'demo-sql'\n" +
                ")");

        Table people = tEnv.from("people");
        people.executeInsert("memory_mysql");
    }
}
